#!/usr/bin/env python3

from lxml import etree
import datetime
import csv
import pandas as pd

current_timestamp = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
current_date = datetime.datetime.now().strftime('%Y-%m-%d')

# create XML 

#set root namespaces
MY_NAMESPACES={'xsi': 'http://www.w3.org/2001/XMLSchema-instance', None: 'http://www.crossref.org/schema/4.4.2', 'jats': 'http://www.ncbi.nlm.nih.gov/JATS1', 'fr': 'http://www.crossref.org/fundref.xsd'}
attr_xsi = etree.QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation")
root = etree.Element('doi_batch', {attr_xsi: "http://www.crossref.org/doi_resources_schema/4.4.2 http://www.crossref.org/schema/deposit/doi_resources4.4.2.xsd"}, nsmap=MY_NAMESPACES, version="4.4.2")

#create head
head = etree.SubElement(root, 'head')

#set head values
doi_batch_id = etree.SubElement(head, 'doi_batch_id').text = "test_1" #TO BE MODIFIED
timestamp = etree.SubElement(head, "timestamp").text = current_timestamp

depositor = etree.SubElement(head, 'depositor')
etree.SubElement(depositor, 'depositor_name').text = "Bononia:bononia" 
etree.SubElement(depositor, 'email_address').text = "journals@buponline.com"

etree.SubElement(head, 'registrant').text = "Bononia"

#create body

body = etree.SubElement(root, 'body')

df = pd.read_csv("esempio.csv", sep=',') 
df.fillna("nan",inplace=True)

for row in df.itertuples():
	book = etree.SubElement(root, 'book', book_type='edited_book') #or monograph
	book_metadata = etree.SubElement(book, 'book_metadata', language="it")
	contributors = etree.SubElement(book_metadata, "contributors")
	name = etree.SubElement(contributors, "person_name", sequence="first", contributor_role="author")
	first_author = row.Autore.split(" ")
	etree.SubElement(name, "given_name").text = first_author[0]
	etree.SubElement(name, "surname").text = " ".join(first_author[1:])
	if row.Autori !='nan':
		list_authors = row.Autori.split(',')
		for el in list_authors[1:]:
			other_author = [x for x in el.split(" ") if x.strip()] #remove empty strings from list (spaces) https://stackoverflow.com/questions/3845423/remove-empty-strings-from-a-list-of-strings
			name_seq = etree.SubElement(contributors, "person_name", sequence="additional", contributor_role="author")
			etree.SubElement(name_seq, "given_name").text = other_author[0]
			etree.SubElement(name_seq, "surname").text = " ".join(other_author[1:])
			#print(name_seq)
	titles = etree.SubElement(book_metadata, "titles")
	etree.SubElement(titles, "title").text = row.Titolo
	jats_namespace = "http://www.ncbi.nlm.nih.gov/JATS1"
	etree.register_namespace("jats", jats_namespace)
	abstract = etree.SubElement(book_metadata, etree.QName(jats_namespace, "abstract"))
	etree.SubElement(abstract, etree.QName(jats_namespace, "p")).text = row.Abstract
	pub_date = etree.SubElement(book_metadata, "publication_date", media_type="print")
	etree.SubElement(pub_date, "year").text = str(row.ANNO)
	etree.SubElement(book_metadata, "isbn", media_type="electronic").text = str(row.ISBN_ONLINE)
	etree.SubElement(book_metadata, "isbn", media_type="print").text = str(row.ISBN)
	publisher = etree.SubElement(book_metadata, "publisher")
	etree.SubElement(publisher, "publisher_name").text = "Bologna University Press"
	doi_data = etree.SubElement(book_metadata, "doi_data")
	etree.SubElement(doi_data, "doi").text = row.DOI
	#etre.SubElement(doi_data, "resource").text = row.Link #change row name
s = etree.tostring(root, pretty_print=True, encoding='utf-8',xml_declaration = True).decode('utf8')
print(s)