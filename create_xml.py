from lxml import etree
import datetime
import csv
import pandas as pd

current_timestamp = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
current_date = datetime.datetime.now().strftime('%Y-%m-%d')

# create XML 

#set root namespaces
MY_NAMESPACES={'xsi': 'http://www.w3.org/2001/XMLSchema-instance', None: 'http://www.crossref.org/schema/4.4.2', 'jats': 'http://www.ncbi.nlm.nih.gov/JATS1', 'fr': 'http://www.crossref.org/fundref.xsd'}
attr_xsi = etree.QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation")
root = etree.Element('doi_batch', {attr_xsi: "http://www.crossref.org/doi_resources_schema/4.4.2 http://www.crossref.org/schema/deposit/doi_resources4.4.2.xsd"}, nsmap=MY_NAMESPACES, version="4.4.2")

#create head
head = etree.SubElement(root, 'head')

#set head values
doi_batch_id = etree.SubElement(head, 'doi_batch_id').text = "test_1" #TO BE MODIFIED
timestamp = etree.SubElement(head, "timestamp").text = current_timestamp

depositor = etree.SubElement(head, 'depositor')
etree.SubElement(depositor, 'depositor_name').text = "Bononia:bononia" 
etree.SubElement(depositor, 'email_address').text = "journals@buponline.com"

etree.SubElement(head, 'registrant').text = "Bononia"

#create body

body = etree.SubElement(root, 'body')

#set body values
journal = etree.SubElement(body, 'journal')
j_data = etree.SubElement(journal, 'journal_metadata', language="it")
etree.SubElement(j_data, "full_title").text = "the journal title" #TO BE MODIFIED
etree.SubElement(j_data, 'issn', media_type='electronic').text = '12345678' #TO BE MODIFIED

doi_data = etree.SubElement(j_data, "doi_data")
etree.SubElement(doi_data, "doi").text = '10.30682/SIGLA' #TO BE MODIFIED
etree.SubElement(doi_data, "resource").text = 'htts://urldelvolumedellarivista' #TO BE MODIFIED

j_issue = etree.SubElement(journal, 'journal_issue')
publication_date = etree.SubElement(j_issue, 'publication_date', media_type='online')
month = etree.SubElement(publication_date, 'month').text = "06" #TO BE MODIFIED
day = etree.SubElement(publication_date, 'day').text = "12" #TO BE MODIFIED
year = etree.SubElement(publication_date, 'year').text = "2022" #TO BE MODIFIED
j_volume = etree.SubElement(j_issue, 'journal_volume')
etree.SubElement(j_volume, 'volume').text = "01" #TO BE MODIFIED
etree.SubElement(j_issue, 'issue').text = "01" #TO BE MODIFIED

#FOR LOOP HERE

df = pd.read_csv("esempio_Tema.tsv", sep='\t') 
#print(df)

for row in df.itertuples():
	#print(row.Title)

	article = etree.SubElement(journal, "journal_article", publication_type="full_text")
	titles = etree.SubElement(article, "titles")
	etree.SubElement(titles, "title").text = row.Title #LOOP
	#in titles it's possible to put also the <original_language_title language="it">
	contributors = etree.SubElement(article, "contributors")
	list_authors = row.Authors.split(',') #already a list in pandas
	#print(list_authors)
	name = etree.SubElement(contributors, "person_name", sequence="first", contributor_role="author")
	first_author = list_authors[0].split(" ")
	etree.SubElement(name, "given_name").text = first_author[0]
	etree.SubElement(name, "surname").text = " ".join(first_author[1:])
	if list_authors[1:] is not None:
		for el in list_authors[1:]:
			other_author = [x for x in el.split(" ") if x.strip()] #remove empty strings from list (spaces) https://stackoverflow.com/questions/3845423/remove-empty-strings-from-a-list-of-strings
			name_seq = etree.SubElement(contributors, "person_name", sequence="additional", contributor_role="author")
			etree.SubElement(name_seq, "given_name").text = other_author[0]
			etree.SubElement(name_seq, "surname").text = " ".join(other_author[1:])
			#print(name_seq)

	jats_namespace = "http://www.ncbi.nlm.nih.gov/JATS1"
	print(row.Abstract)
	if row.isnull():
		pass
	else:
		etree.register_namespace("jats", jats_namespace)
		abstract = etree.SubElement(article, etree.QName(jats_namespace, "abstract"))
		etree.SubElement(abstract, etree.QName(jats_namespace, "p")).text = row.Abstract

	publication = etree.SubElement(article, "publication_date", media_type="online")
	etree.SubElement(publication, "month"). text = month
	etree.SubElement(publication, "day").text = day
	etree.SubElement(publication, "year").text = year

	license_namespace = {None: "http://www.crossref.org/AccessIndicators.xsd"}
	license = etree.SubElement(article, "program", nsmap=license_namespace)
	etree.SubElement(license, "free_to_read")
	etree.SubElement(license, "license_ref", applies_to="vor", start_date=current_date).text = "https://creativecommons.org/licenses/by/4.0/"


	article_doi_data = etree.SubElement(article, "doi_data")
	etree.SubElement(article_doi_data, "doi").text = row.DOI
	etree.SubElement(article_doi_data, "resource", content_version="vor").text = row.Link 

	# pretty string
s = etree.tostring(root, pretty_print=True, encoding='utf-8',xml_declaration = True).decode('utf8')
print(s)

#must be UTF-8 encoded

