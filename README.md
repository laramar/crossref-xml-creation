# Crossref Xml Creation

A python script to create XML based on articles, to ingest in Crossref. 
See XML examples [in the Schema gitlab project](https://gitlab.com/crossref/schema)


Version of XML: 4.4.2
Required metadata [from Corssref documentation](https://www.crossref.org/documentation/schema-library/required-recommended-elements/) 

## Notes on XML

Reference: [Schema documentation](https://data.crossref.org/reports/help/schema_doc/4.4.2/index.html)

- doi_ batch_id: Publisher generated ID that uniquely identifies the DOI submission batch. It will be used as a reference in error messages sent by the MDDB, and can be used for submission tracking. The publisher must ensure that this number is unique for every submission to Crossref.
- doi_data: Doi for the single articles (inside each "journal _article" set). There is the doi for each article and the relative url (resource) 

<doi_data>
<doi>10.32013/y2uGhyC</doi>
<resource>https://www.crossref.org/xml-samples/</resource>

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## Authors and acknowledgment
Lara Marziali - Bologna University Press

## License
GNU General Public License v3.0

## Project status
Just beginning
